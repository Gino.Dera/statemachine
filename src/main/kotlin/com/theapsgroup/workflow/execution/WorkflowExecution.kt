package com.theapsgroup.workflow.execution

import com.theapsgroup.workflow.core.*

abstract class WorkflowExecution(
    internal val delayedActions: HashMap<WorkflowDelayedAction, ArrayList<() -> Unit>> = hashMapOf(),
    internal val messages: ArrayList<WorkflowMessage> = arrayListOf(),
    internal val notifications: HashMap<WorkflowState, HashSet<String>> = hashMapOf()
) {
    fun notify(state: WorkflowState, signal: WorkflowSignal) {
        if (!notifications.containsKey(state)) {
            notifications[state] = hashSetOf()
        }

        notifications[state]?.add(WorkflowItem.extract(signal))
    }

    fun output(message: WorkflowMessage) {
        messages.add(message)
    }

    fun delay(action: WorkflowDelayedAction, func: () -> Unit) {
        if (!delayedActions.containsKey(action)) {
            delayedActions[action] = arrayListOf()
        }

        delayedActions[action]?.add(func)
    }
}