package com.theapsgroup.workflow.execution

import com.theapsgroup.workflow.context.WorkflowBranch
import com.theapsgroup.workflow.context.WorkflowContext
import com.theapsgroup.workflow.core.*

data class WorkflowStepExecution(
    internal val context: WorkflowContext,
    internal val branch: WorkflowBranch,
) : WorkflowExecution() {
    fun bind(state: WorkflowState) {
        if (context.boundState != null) {
            throw Exception("Context cannot have its state rebound")
        }

        context.boundState = state.id
    }

    fun done(): StepResult {
        return StepResult.Done()
    }

    fun observe(signals: HashMap<WorkflowState, List<WorkflowSignal>>, step: WorkflowStep): StepResult {
        return StepResult.Observe(signals, step)
    }

    fun observe(state: WorkflowState, signal: WorkflowSignal, step: WorkflowStep): StepResult {
        return StepResult.Observe(hashMapOf(state to listOf(signal)), step)
    }

    fun next(step: WorkflowStep): StepResult {
        return StepResult.Next(step)
    }

    @JvmName("parallelSteps")
    fun parallel(steps: List<WorkflowStep>): StepResult.Parallel {
        return StepResult.Parallel(listOf(), steps)
    }

    @JvmName("parallelSignals")
    fun parallel(signals: List<ParallelStateSignals>): StepResult.Parallel {
        return StepResult.Parallel(signals, listOf())
    }

    fun parallel(
        signals: List<ParallelStateSignals>,
        steps: List<WorkflowStep>,
    ): StepResult.Parallel {
        return StepResult.Parallel(signals, steps)
    }
}