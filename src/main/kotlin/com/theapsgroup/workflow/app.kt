package com.theapsgroup.workflow

import com.fasterxml.jackson.annotation.JsonProperty
import com.theapsgroup.workflow.MongoResourceStateRepository.Companion.findState
import com.theapsgroup.workflow.MongoResourceStateRepository.Companion.newState
import com.theapsgroup.workflow.async.CoroutineBackgroundTaskRunner
import com.theapsgroup.workflow.core.*
import com.theapsgroup.workflow.impl.*
import org.litote.kmongo.KMongo
import java.util.*
import kotlin.concurrent.thread

// Variables
const val waitForActivity = false

// Events
@WorkflowItem("JobCreatedEvent")
object JobCreatedEvent : WorkflowMessage

@WorkflowItem("TaskCreatedEvent")
data class TaskCreatedEvent(
    @JsonProperty("state")
    val state: String
) : WorkflowMessage

@WorkflowItem("ApprovalCreatedEvent")
object ApprovalCreatedEvent : WorkflowMessage

@WorkflowItem("StorageCreatedEvent")
data class StorageCreatedEvent(
    @JsonProperty("state")
    val state: String
) : WorkflowMessage

@WorkflowItem("ExternalLinkSetEvent")
object ExternalLinkSetEvent : WorkflowMessage

// Steps
@WorkflowItem("CreateTaskStep")
object CreateTaskStep : WorkflowStep

@WorkflowItem("CreateStorageStep")
object CreateStorageStep : WorkflowStep

@WorkflowItem("DoneStep")
object DoneStep : WorkflowStep

// Signals
@WorkflowItem("ShouldCreateTaskSignal")
object ShouldCreateTaskSignal : WorkflowSignal

@WorkflowItem("TaskBoundToStateSignal")
object TaskBoundToStateSignal : WorkflowSignal

@WorkflowItem("StorageBoundToStateSignal")
object StorageBoundToStateSignal : WorkflowSignal

@WorkflowItem("ApprovalBoundToStateSignal")
object ApprovalBoundToStateSignal : WorkflowSignal

// State
class ResourceState(
    @JsonProperty("id")
    override val id: String = UUID.randomUUID().toString(),
) : WorkflowState

// Workflow
class TaskCreatedWorkflow : Workflow<TaskCreatedEvent> {
    override fun configure() {
        start { execution, event ->
            val state = execution.findState(event.state)

            execution.notify(state, TaskBoundToStateSignal)

            return@start execution.done()
        }
    }
}

class StorageCreatedWorkflow : Workflow<StorageCreatedEvent> {
    override fun configure() {
        start { execution, event ->
            val state = execution.findState(event.state)

            execution.notify(state, StorageBoundToStateSignal)

            return@start execution.done()
        }
    }
}

class JobCreatedWorkflow : Workflow<JobCreatedEvent> {
    override fun configure() {
        start { execution, event ->
            val state = execution.newState()

            execution.output(TaskCreatedEvent(state.id))
            execution.output(StorageCreatedEvent(state.id))

            return@start execution.parallel(
                listOf(
                    state to listOf(
                        CreateTaskStep to listOf(
                            TaskBoundToStateSignal
                        ),
                        CreateStorageStep to listOf(
                            StorageBoundToStateSignal
                        )
                    )
                )
            )

//            return@start execution.parallel(
//                listOf(
//                    CreateTaskStep,
//                    CreateStorageStep
//                )
//            )
//
//            return@start execution.observe(
//                hashMapOf(
//                    state to listOf(
//                        TaskBoundToStateSignal,
//                        StorageBoundToStateSignal
//                    )
//                ),
//                DoneStep
//            )
//            if (waitForActivity) {
//                return@start execution.observe(state, ShouldCreateTaskSignal, CreateTaskStep)
//            } else {
//                return@start execution.parallel(
//                    state,
//                    steps = listOf(
//                        CreateStorageStep
//                    ),
//                    waitingOn = hashMapOf(
//                        CreateTaskStep to listOf(
//                            ShouldCreateTaskSignal
//                        )
//                    )
//                )
//            }
        }

        step(CreateTaskStep) { execution, event ->
//            execution.output(TaskCreatedEvent(state.id))

            return@step execution.next(DoneStep)
        }

        step(CreateStorageStep) { execution, event ->
//            execution.output(StorageCreatedEvent(state.id))

            return@step execution.next(DoneStep)
        }

        step(DoneStep) { execution, event ->
            return@step execution.done()
        }
    }
}

fun main() {
    StorageCreatedWorkflow().initialize()
    JobCreatedWorkflow().initialize()
    TaskCreatedWorkflow().initialize()

    val client = KMongo.createClient()

    val contextRepository = MongoWorkflowContextRepository(client)
    val signalRepository = MongoWorkflowSignalRepository(client)
    val stateRepository = MongoResourceStateRepository(client)

    val messageBus = MongoWorkflowMessageBus(contextRepository)

    thread {
        for (i in 0..100) {
            messageBus.publish(JobCreatedEvent)
        }
    }
//    messageBus.publish(TaskCreatedEvent("98efe83e-a459-4241-ba2c-b8c7ebc81daa"))

    val contextProcessor = DefaultWorkflowContextProcessor()
    val contextWorker =
        DefaultWorkflowContextWorker(contextProcessor, contextRepository, signalRepository, messageBus)
    val contextWorkerRunner = CoroutineBackgroundTaskRunner(contextWorker).also {
        it.start()
    }


    val signalProcessor = DefaultWorkflowSignalProcessor()
    val signalWorker =
        DefaultWorkflowSignalWorker(signalProcessor, contextRepository, signalRepository, stateRepository, messageBus)
    val signalWorkerRunner = CoroutineBackgroundTaskRunner(signalWorker).also {
        it.start()
    }

    readln()
}