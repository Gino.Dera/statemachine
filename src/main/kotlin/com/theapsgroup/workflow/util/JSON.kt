package com.theapsgroup.workflow.util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.nio.charset.Charset
import kotlin.reflect.KClass

class JSON private constructor() {
    private val mapper = ObjectMapper()
        .registerKotlinModule()

    fun marshalString(value: Any): String {
        return mapper.writeValueAsString(value)
    }

    fun marshalByteArray(value: Any): ByteArray {
        return mapper.writeValueAsBytes(value)
    }

    fun marshalNode(value: Any): JsonNode {
        return mapper.valueToTree(value)
    }

    fun <T : Any> unmarshal(valueType: KClass<T>, value: String): T {
        try {
            return mapper.readValue(value, valueType.java)
        } catch (e: Exception) {
            throw Exception(
                "Error while trying to unmarshal source to ${valueType.simpleName}, source: $value"
            )
        }
    }

    fun <T : Any> unmarshal(valueType: KClass<T>, value: ByteArray): T {
        try {
            return mapper.readValue(value, valueType.java)
        } catch (e: Exception) {
            throw Exception(
                "Error while trying to unmarshal source to ${valueType.simpleName}, source: ${
                    value.toString(
                        Charset.defaultCharset()
                    )
                }"
            )
        }
    }

    fun <T : Any> unmarshal(valueType: KClass<T>, value: JsonNode): T {
        try {
            return mapper.treeToValue(value, valueType.java)
        } catch (e: Exception) {
            throw Exception(
                "Error while trying to unmarshal source to ${valueType.simpleName}, source: ${
                    value.toPrettyString()
                }"
            )
        }
    }

    fun unmarshal(value: String): JsonNode {
        try {
            return mapper.readTree(value)
        } catch (e: Exception) {
            throw Exception(
                "Error while trying to unmarshal source to JsonNode. Source: $value"
            )
        }
    }


    fun <T : Any> convert(valueType: KClass<T>, value: Any): T {
        try {
            return mapper.convertValue(value, valueType.java)
        } catch (e: Exception) {
            throw Exception(
                "Error while trying to convert source to ${valueType.simpleName}, source: $value"
            )
        }
    }

    companion object {
        private val instance = JSON()

        fun marshalString(value: Any): String =
            instance.marshalString(value)

        fun marshalByteArray(value: Any): ByteArray =
            instance.marshalByteArray(value)

        fun marshalNode(value: Any): JsonNode =
            instance.marshalNode(value)

        inline fun <reified T : Any> unmarshal(value: String): T =
            unmarshal(T::class, value)

        fun <T : Any> unmarshal(valueType: KClass<T>, value: String): T =
            instance.unmarshal(valueType, value)

        fun <T : Any> unmarshal(valueType: KClass<T>, value: JsonNode): T =
            instance.unmarshal(valueType, value)

        inline fun <reified T : Any> unmarshal(value: ByteArray): T =
            unmarshal(T::class, value)

        fun <T : Any> unmarshal(valueType: KClass<T>, value: ByteArray): T =
            instance.unmarshal(valueType, value)

        fun unmarshal(value: String): JsonNode =
            instance.unmarshal(value)

        inline fun <reified T : Any> convert(value: Any): T =
            convert(T::class, value)

        fun <T : Any> convert(valueType: KClass<T>, value: Any): T =
            instance.convert(valueType, value)
    }
}
