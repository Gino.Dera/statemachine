package com.theapsgroup.workflow.signal

import com.theapsgroup.workflow.core.WorkflowState

interface WorkflowSignalRepository {
    fun getAvailable(): WorkflowSignal?
    fun create(signals: HashMap<WorkflowState, HashSet<String>>)
    fun remove(signal: WorkflowSignal)
}