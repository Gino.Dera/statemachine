package com.theapsgroup.workflow.signal

interface WorkflowSignal {
    val id: String
    val state: String
    val name: String
}