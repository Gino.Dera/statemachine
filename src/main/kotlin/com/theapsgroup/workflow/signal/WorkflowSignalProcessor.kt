package com.theapsgroup.workflow.signal

import com.theapsgroup.workflow.core.WorkflowState
import com.theapsgroup.workflow.execution.WorkflowSignalExecution

interface WorkflowSignalProcessor {
    fun process(execution: WorkflowSignalExecution, signal: WorkflowSignal, state: WorkflowState)
}