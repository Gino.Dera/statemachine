package com.theapsgroup.workflow

import com.mongodb.client.MongoClient
import com.mongodb.client.model.ReplaceOptions
import com.theapsgroup.workflow.core.DuringContextSaving
import com.theapsgroup.workflow.core.WorkflowState
import com.theapsgroup.workflow.core.WorkflowStateRepository
import com.theapsgroup.workflow.execution.WorkflowStepExecution
import org.litote.kmongo.eq
import org.litote.kmongo.findOne

class MongoResourceStateRepository(
    client: MongoClient
) : WorkflowStateRepository {
    private val database = client.getDatabase("sample")
    private val collection = database.getCollection("states", ResourceState::class.java)

    init {
        instance = this
    }

    override fun get(id: String): WorkflowState? {
        return collection.findOne(
            ResourceState::id eq id
        )
    }

    override fun save(state: WorkflowState) {
        collection.replaceOne(
            ResourceState::id eq state.id,
            state as ResourceState,
            ReplaceOptions().also {
                it.upsert(true)
            }
        )
    }

    fun create(): ResourceState {
        return ResourceState()
    }

    companion object {
        lateinit var instance: MongoResourceStateRepository

        fun WorkflowStepExecution.newState(): ResourceState {
            return instance.create().also {
                bind(it)
                delay(DuringContextSaving) {
                    instance.save(it)
                }
            }
        }

        fun WorkflowStepExecution.getState(): ResourceState {
            if (context.boundState == null) {
                throw Exception("Workflow can only retrieve state if one is bound to the current context")
            }

            return instance.get(context.boundState!!) as ResourceState
        }

        fun WorkflowStepExecution.findState(id: String): ResourceState {
            return instance.get(id) as? ResourceState ?: throw Exception(
                "Could not find state for id: $id"
            )
        }
    }
}