package com.theapsgroup.workflow.async

interface BackgroundTaskRunner {
    fun start()
    fun stop()
    fun isRunning(): Boolean
}