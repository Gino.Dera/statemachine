package com.theapsgroup.workflow.async

import kotlinx.coroutines.*

class CoroutineBackgroundTaskRunner(
    private val backgroundTask: BackgroundTask,
    private val backgroundTaskName: String? = null
) : BackgroundTaskRunner {
    private var job: Job? = null

    override fun start() {
        job = CoroutineScope(Dispatchers.IO.apply {
            if (backgroundTaskName != null) {
                plus(CoroutineName(backgroundTaskName))
            }
        }).async {
            run()
        }
    }

    private suspend fun run() {
        withContext(Dispatchers.IO) {
            while (isRunning()) {
                try {
                    backgroundTask.perform()
                } catch (e: Exception) {
                    e.printStackTrace()
                    delay(5000)
                }
            }
        }
    }

    override fun stop() {
        runBlocking {
            job?.cancelAndJoin()
        }
    }

    override fun isRunning(): Boolean {
        return job?.isActive ?: false
    }
}