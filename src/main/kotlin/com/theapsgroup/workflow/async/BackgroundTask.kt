package com.theapsgroup.workflow.async

interface BackgroundTask {
    suspend fun perform()
}