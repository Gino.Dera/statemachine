package com.theapsgroup.workflow.bus

import com.theapsgroup.workflow.core.WorkflowMessage

interface WorkflowMessageBus {
    fun publish(message: WorkflowMessage)
}