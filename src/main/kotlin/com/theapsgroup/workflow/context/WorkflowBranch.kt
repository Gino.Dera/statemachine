package com.theapsgroup.workflow.context

interface WorkflowBranch {
    val id: String
    val parent: String?
    val level: Int
    var status: WorkflowContext.Status
    var nextStep: String?
    val attributes: HashMap<String, Any>
    val waitingOn: ArrayList<WaitingOn>
    val history: ArrayList<String>

    data class WaitingOn(
        val state: String,
        val signal: String
    )
}