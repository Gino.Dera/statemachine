package com.theapsgroup.workflow.context

import com.theapsgroup.workflow.core.WorkflowMessage
import com.theapsgroup.workflow.core.WorkflowState
import com.theapsgroup.workflow.signal.WorkflowSignal

interface WorkflowContextRepository {
    fun getAvailable(): WorkflowContext?
    fun save(context: WorkflowContext)
    fun delete(context: WorkflowContext)
    fun createFor(message: WorkflowMessage)
    fun resolveWaitingOn(signal: WorkflowSignal, state: WorkflowState)
}