package com.theapsgroup.workflow.context

interface WorkflowContext {
    val id: String
    val type: String
    val payload: String
    var boundState: String?
    val branches: ArrayList<WorkflowBranch>

    enum class Status {
        New,
        Processing,
        Branched,
        Done
    }
}