package com.theapsgroup.workflow.context

import com.theapsgroup.workflow.core.WorkflowMessage
import com.theapsgroup.workflow.execution.WorkflowStepExecution

interface WorkflowContextProcessor {
    fun process(
        execution: WorkflowStepExecution,
        context: WorkflowContext,
        branch: WorkflowBranch,
        message: WorkflowMessage
    )
}