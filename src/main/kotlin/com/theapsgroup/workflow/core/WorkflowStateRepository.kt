package com.theapsgroup.workflow.core

interface WorkflowStateRepository {
    fun get(id: String): WorkflowState?
    fun save(state: WorkflowState)
}