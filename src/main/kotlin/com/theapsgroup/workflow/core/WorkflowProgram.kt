package com.theapsgroup.workflow.core

import com.theapsgroup.workflow.context.WorkflowContext
import com.theapsgroup.workflow.execution.WorkflowStepExecution

class WorkflowProgram<TMessage : WorkflowMessage>(
    private val definition: Workflow<TMessage>
) {
    private lateinit var startFunction: WorkflowFunction<TMessage>
    private val stepFunctions: HashMap<String, WorkflowFunction<TMessage>> = hashMapOf()

    internal fun setStartFunction(func: WorkflowFunction<TMessage>) {
        startFunction = func
    }

    internal fun addStepFunction(id: String, func: WorkflowFunction<TMessage>) {
        stepFunctions[id] = func
    }

    fun execute(execution: WorkflowStepExecution, message: TMessage): StepResult {
        if (execution.branch.status == WorkflowContext.Status.New) {
//            println("[START] ${definition::class.simpleName}")
            return startFunction(execution, message)
        }

        if (execution.branch.nextStep == null) {
            println("Workflow context does not have any next step to execute, marking it as done")
            return execution.done()
        }

        val func = stepFunctions[execution.branch.nextStep] ?: throw Exception(
            "Could not find step to execute. Workflow: ${definition::class.simpleName}, Step: ${execution.branch.nextStep}"
        )

//        println("[STEP] ${definition::class.simpleName}:${execution.branch.nextStep}")
        return func(execution, message)
    }
}