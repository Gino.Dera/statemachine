package com.theapsgroup.workflow.core

import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

@Target(AnnotationTarget.CLASS)
annotation class WorkflowItem(
    val id: String
) {
    companion object {
        inline fun <reified T : Any> extract(): String {
            return extract(T::class)
        }

        fun extract(c: Any): String {
            return extract(c::class)
        }

        fun extract(c: KClass<*>): String {
            val annotation = c.findAnnotation<WorkflowItem>() ?: throw Exception(
                "Workflow steps should be annotated with @${WorkflowItem::class.simpleName}, missing on: ${this::class.simpleName}"
            )

            return annotation.id
        }
    }
}