package com.theapsgroup.workflow.core

import com.theapsgroup.workflow.execution.WorkflowSignalExecution
import com.theapsgroup.workflow.execution.WorkflowStepExecution
import kotlin.reflect.KClass

// -----------------------------------
// Interfaces
// -----------------------------------
interface WorkflowMessage

interface WorkflowStep

interface WorkflowSignal

interface WorkflowState {
    val id: String
}

interface WorkflowDelayedAction

interface StepResult {
    class Done internal constructor(
    ) : StepResult

    class Next internal constructor(
        val step: WorkflowStep
    ) : StepResult

    data class Observe internal constructor(
        val signals: HashMap<WorkflowState, List<WorkflowSignal>>,
        val step: WorkflowStep
    ) : StepResult

    data class Parallel internal constructor(
        val signals: List<ParallelStateSignals>,
        val steps: List<WorkflowStep>
    ) : StepResult
}

@Suppress("UNCHECKED_CAST")
interface Workflow<T : WorkflowMessage> {
    fun configure()

    fun getMessageType(): KClass<out WorkflowMessage> {
        val type = this::class.supertypes.first().arguments.first().type!!
        return type.classifier as KClass<out WorkflowMessage>
    }
}

interface SignalListener<T : WorkflowState> {
    fun configure()
}

// -----------------------------------
// Type aliases
// -----------------------------------
typealias SignalListenerFunction<T> = (execution: WorkflowSignalExecution, state: T) -> Unit
typealias WorkflowFunction<T> = (execution: WorkflowStepExecution, message: T) -> StepResult
typealias ParallelStateSignals = Pair<WorkflowState, List<Pair<WorkflowStep, List<WorkflowSignal>>>>

// -----------------------------------
// Data classes
// -----------------------------------
data class SignalListenerDefinition<T : WorkflowState>(
    val listener: SignalListener<T>,
    val func: SignalListenerFunction<in T>
)

// -----------------------------------
// Objects
// -----------------------------------
object DuringContextSaving : WorkflowDelayedAction

// -----------------------------------
// Variables
// -----------------------------------
val messageTypes = hashMapOf<String, KClass<out WorkflowMessage>>()
val workflowPrograms = hashMapOf<KClass<out WorkflowMessage>, WorkflowProgram<in WorkflowMessage>>()
val signalListeners = hashMapOf<String, ArrayList<SignalListenerDefinition<WorkflowState>>>()

// -----------------------------------
// Extension functions
// -----------------------------------
@Suppress("UNCHECKED_CAST")
fun <T : WorkflowMessage> Workflow<T>.initialize() {
    getMessageType().also {
        if (workflowPrograms.containsKey(it)) {
            throw Exception("Workflow already exists that handles message: ${it.simpleName}")
        }

        messageTypes[WorkflowItem.extract(it)] = it
        workflowPrograms[it] = WorkflowProgram(this) as WorkflowProgram<in WorkflowMessage>

        configure()
    }
}

@Suppress("UNCHECKED_CAST")
fun <T : WorkflowMessage> Workflow<T>.start(func: WorkflowFunction<T>) {
    getMessageType().also {
        if (!workflowPrograms.containsKey(it)) {
            throw Exception("Workflow not in configuration state: ${this::class.simpleName}")
        }

        (workflowPrograms[it] as WorkflowProgram<T>).setStartFunction(func)
    }
}

@Suppress("UNCHECKED_CAST")
fun <T : WorkflowMessage> Workflow<T>.step(step: WorkflowStep, func: WorkflowFunction<T>) {
    getMessageType().also {
        if (!workflowPrograms.containsKey(it)) {
            throw Exception("Workflow not in configuration state: ${this::class.simpleName}")
        }

        (workflowPrograms[it] as WorkflowProgram<T>).addStepFunction(WorkflowItem.extract(step), func)
    }
}

fun <T : WorkflowState> SignalListener<T>.initialize() {
    configure()
}

@Suppress("UNCHECKED_CAST")
fun <T : WorkflowState> SignalListener<T>.observe(signal: WorkflowSignal, func: SignalListenerFunction<T>) {
    val name = WorkflowItem.extract(signal)
    if (!signalListeners.containsKey(name)) {
        signalListeners[name] = arrayListOf()
    }

    signalListeners[name]?.add(
        SignalListenerDefinition(
            this as SignalListener<WorkflowState>,
            func as SignalListenerFunction<in WorkflowState>,
        )
    )
}