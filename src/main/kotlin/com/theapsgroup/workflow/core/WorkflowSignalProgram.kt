package com.theapsgroup.workflow.core

import com.theapsgroup.workflow.execution.WorkflowSignalExecution
import com.theapsgroup.workflow.signal.WorkflowSignal

class WorkflowSignalProgram<TState : WorkflowState>(
    private val definition: SignalListener<TState>
) {
    private val observeFunctions: HashMap<String, SignalListenerFunction<TState>> = hashMapOf()

    internal fun addObserveFunction(id: String, func: SignalListenerFunction<TState>) {
        observeFunctions[id] = func
    }

    fun execute(execution: WorkflowSignalExecution, signal: WorkflowSignal, state: TState) {
        val func = observeFunctions[signal.name] ?: throw Exception(
            "Could not find signal to execute. Listener: ${definition::class.simpleName}, Signal: ${signal.name}"
        )

//        println("[SIGNAL] ${definition::class.simpleName}:${signal.name}")

        func(execution, state)
    }
}