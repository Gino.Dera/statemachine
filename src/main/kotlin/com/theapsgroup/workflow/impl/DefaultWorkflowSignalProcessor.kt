package com.theapsgroup.workflow.impl

import com.theapsgroup.workflow.core.WorkflowState
import com.theapsgroup.workflow.core.signalListeners
import com.theapsgroup.workflow.execution.WorkflowSignalExecution
import com.theapsgroup.workflow.signal.WorkflowSignal
import com.theapsgroup.workflow.signal.WorkflowSignalProcessor

class DefaultWorkflowSignalProcessor : WorkflowSignalProcessor {
    override fun process(execution: WorkflowSignalExecution, signal: WorkflowSignal, state: WorkflowState) {
        signalListeners[signal.name]?.forEach { definition ->
//            println("[SIGNAL] ${definition.listener::class.simpleName}:${signal.name}")

            definition.func.invoke(execution, state)
        }
    }
}