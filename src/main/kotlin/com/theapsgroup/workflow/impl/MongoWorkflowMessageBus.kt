package com.theapsgroup.workflow.impl

import com.theapsgroup.workflow.bus.WorkflowMessageBus
import com.theapsgroup.workflow.core.WorkflowMessage

class MongoWorkflowMessageBus(
    private val contextRepository: MongoWorkflowContextRepository
) : WorkflowMessageBus {
    override fun publish(message: WorkflowMessage) {
        contextRepository.createFor(message)
    }
}