package com.theapsgroup.workflow.impl

import com.theapsgroup.workflow.context.WorkflowBranch
import com.theapsgroup.workflow.context.WorkflowContext
import com.theapsgroup.workflow.context.WorkflowContextProcessor
import com.theapsgroup.workflow.core.StepResult
import com.theapsgroup.workflow.core.WorkflowItem
import com.theapsgroup.workflow.core.WorkflowMessage
import com.theapsgroup.workflow.core.workflowPrograms
import com.theapsgroup.workflow.execution.WorkflowStepExecution

class DefaultWorkflowContextProcessor : WorkflowContextProcessor {
    override fun process(
        execution: WorkflowStepExecution,
        context: WorkflowContext,
        branch: WorkflowBranch,
        message: WorkflowMessage
    ) {
        val program = workflowPrograms[message::class] ?: throw Exception(
            "No workflow registered to handle: ${message::class.simpleName}"
        )

        branch.history.add(branch.nextStep ?: "StartStep")

        when (val result = program.execute(execution, message)) {
            is StepResult.Done -> {
                completeBranch(branch)
            }

            is StepResult.Next -> {
                branch.status = WorkflowContext.Status.Processing
                branch.nextStep = WorkflowItem.extract(result.step)
            }

            is StepResult.Observe -> {
                branch.status = WorkflowContext.Status.Processing
                branch.nextStep = WorkflowItem.extract(result.step)

                result.signals.forEach { (state, signals) ->
                    signals.forEach { signal ->
                        branch.waitingOn.add(
                            WorkflowBranch.WaitingOn(
                                state.id,
                                WorkflowItem.extract(signal)
                            )
                        )
                    }
                }
            }

            is StepResult.Parallel -> {
                completeBranch(branch)

                result.steps.forEach { step ->
                    context.branches.add(
                        MongoWorkflowContextRepository.MongoWorkflowBranch(
                            status = WorkflowContext.Status.Branched,
                            parent = branch.id,
                            level = branch.level + 1,
                            nextStep = WorkflowItem.extract(step),
                            attributes = branch.attributes,
                            history = branch.history
                        )
                    )
                }

                result.signals.forEach { (state, steps) ->
                    steps.forEach { (step, signals) ->
                        context.branches.add(
                            MongoWorkflowContextRepository.MongoWorkflowBranch(
                                status = WorkflowContext.Status.Branched,
                                parent = branch.id,
                                level = branch.level + 1,
                                nextStep = WorkflowItem.extract(step),
                                attributes = branch.attributes,
                                waitingOn = signals.mapTo(arrayListOf()) { signal ->
                                    WorkflowBranch.WaitingOn(
                                        state.id,
                                        WorkflowItem.extract(signal)
                                    )
                                },
                                history = branch.history
                            )
                        )
                    }
                }
            }
        }
    }

    private fun completeBranch(branch: WorkflowBranch) {
        branch.status = WorkflowContext.Status.Done
        branch.nextStep = null
    }
}