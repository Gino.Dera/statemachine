package com.theapsgroup.workflow.impl

import com.fasterxml.jackson.annotation.JsonProperty
import com.mongodb.client.MongoClient
import com.mongodb.client.model.Filters
import com.mongodb.client.model.FindOneAndUpdateOptions
import com.mongodb.client.model.ReturnDocument
import com.theapsgroup.workflow.core.WorkflowState
import com.theapsgroup.workflow.signal.WorkflowSignal
import com.theapsgroup.workflow.signal.WorkflowSignalRepository
import org.litote.kmongo.*
import java.time.Duration
import java.time.Instant
import java.util.*

class MongoWorkflowSignalRepository(
    client: MongoClient
) : WorkflowSignalRepository {
    data class MongoWorkflowSignal(
        @JsonProperty("state")
        override val state: String,

        @JsonProperty("signal")
        override val name: String,

        @JsonProperty("id")
        override val id: String = UUID.randomUUID().toString(),

        @JsonProperty("unlockAt")
        val unlockAt: Instant? = null,

        @JsonProperty("createdAt")
        var createdAt: Instant = Instant.now(),
    ) : WorkflowSignal

    private val database = client.getDatabase("sample")
    private val collection = database.getCollection("signals", MongoWorkflowSignal::class.java)

    override fun getAvailable(): WorkflowSignal? {
        return collection.findOneAndUpdate(
            or(
                and(
                    MongoWorkflowSignal::unlockAt exists true,
                    MongoWorkflowSignal::unlockAt eq null,
                ),
                MongoWorkflowSignal::unlockAt lte Instant.now()
            ),
            setValue(MongoWorkflowSignal::unlockAt, Instant.now().plus(Duration.ofMinutes(1))),
            FindOneAndUpdateOptions().also {
                it.returnDocument(ReturnDocument.BEFORE)
            }
        )
    }

    override fun create(signals: HashMap<WorkflowState, HashSet<String>>) {
        collection.insertMany(
            signals.flatMap { (state, names) ->
                names.map { name ->
                    MongoWorkflowSignal(state.id, name)
                }
            }
        )
    }

    override fun remove(signal: WorkflowSignal) {
        collection.deleteOne(
            MongoWorkflowSignal::id eq signal.id
        )
    }
}