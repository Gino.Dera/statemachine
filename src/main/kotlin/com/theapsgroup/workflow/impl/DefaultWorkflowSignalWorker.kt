package com.theapsgroup.workflow.impl

import com.theapsgroup.workflow.async.BackgroundTask
import com.theapsgroup.workflow.bus.WorkflowMessageBus
import com.theapsgroup.workflow.context.WorkflowContextRepository
import com.theapsgroup.workflow.core.DuringContextSaving
import com.theapsgroup.workflow.core.WorkflowStateRepository
import com.theapsgroup.workflow.execution.WorkflowSignalExecution
import com.theapsgroup.workflow.signal.WorkflowSignalProcessor
import com.theapsgroup.workflow.signal.WorkflowSignalRepository
import kotlinx.coroutines.delay

class DefaultWorkflowSignalWorker(
    private val processor: WorkflowSignalProcessor,
    private val contextRepository: WorkflowContextRepository,
    private val signalRepository: WorkflowSignalRepository,
    private val stateRepository: WorkflowStateRepository,
    private val bus: WorkflowMessageBus
) : BackgroundTask {
    override suspend fun perform() {
        val signal = signalRepository.getAvailable()
        if (signal == null) {
            delay(1000)
            return
        }

        val state = stateRepository.get(signal.state) ?: throw Exception(
            "Could not find state for signal"
        )

        val execution = WorkflowSignalExecution()
        processor.process(execution, signal, state)

        if (execution.notifications.isNotEmpty()) {
            signalRepository.create(execution.notifications)
        }

        execution.messages.forEach {
            bus.publish(it)
        }

        execution.delayedActions[DuringContextSaving]?.forEach { func ->
            func()
        }

        contextRepository.resolveWaitingOn(signal, state)
//        signalRepository.remove(signal)
    }
}