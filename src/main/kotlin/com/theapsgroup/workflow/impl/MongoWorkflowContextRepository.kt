package com.theapsgroup.workflow.impl

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.mongodb.client.MongoClient
import com.mongodb.client.model.ReplaceOptions
import com.theapsgroup.workflow.context.WorkflowBranch
import com.theapsgroup.workflow.context.WorkflowContext
import com.theapsgroup.workflow.context.WorkflowContextRepository
import com.theapsgroup.workflow.core.WorkflowItem
import com.theapsgroup.workflow.core.WorkflowMessage
import com.theapsgroup.workflow.core.WorkflowState
import com.theapsgroup.workflow.signal.WorkflowSignal
import com.theapsgroup.workflow.util.JSON
import org.litote.kmongo.*
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.reflect.KProperty1

class MongoWorkflowContextRepository(
    client: MongoClient
) : WorkflowContextRepository {
    data class MongoWorkflowContext(
        @JsonProperty("type")
        override val type: String,

        @JsonProperty("payload")
        override val payload: String,

        @JsonProperty("boundState")
        override var boundState: String? = null,

        @JsonProperty("branches")
        @JsonDeserialize(contentAs = MongoWorkflowBranch::class)
        override val branches: ArrayList<WorkflowBranch> = arrayListOf(
            MongoWorkflowBranch(null)
        ),

        @JsonProperty("id")
        override val id: String = UUID.randomUUID().toString(),

        @JsonProperty("createdAt")
        var createdAt: Instant = Instant.now(),

        @JsonProperty("updatedAt")
        var updatedAt: Instant = Instant.now(),
    ) : WorkflowContext

    data class MongoWorkflowBranch(
        @JsonProperty("parent")
        override val parent: String?,

        @JsonProperty("level")
        override val level: Int = 0,

        @JsonProperty("status")
        override var status: WorkflowContext.Status = WorkflowContext.Status.New,

        @JsonProperty("nextStep")
        override var nextStep: String? = null,

        @JsonProperty("attributes")
        override val attributes: HashMap<String, Any> = hashMapOf(),

        @JsonProperty("waitingOn")
        override val waitingOn: ArrayList<WorkflowBranch.WaitingOn> = arrayListOf(),

        @JsonProperty("history")
        override val history: ArrayList<String> = arrayListOf(),

        @JsonProperty("unlockAt")
        val unlockAt: Instant? = null,

        @JsonProperty("id")
        override val id: String = UUID.randomUUID().toString(),
    ) : WorkflowBranch

    private val database = client.getDatabase("sample")
    private val collection = database.getCollection("contexts", MongoWorkflowContext::class.java)

    init {
        collection.ensureIndex(MongoWorkflowContext::id)
        collection.ensureIndex(MongoWorkflowContext::branches / MongoWorkflowBranch::status)
    }

    override fun getAvailable(): WorkflowContext? {
        return collection.findOneAndUpdate(
            MongoWorkflowContext::branches elemMatch and(
                MongoWorkflowBranch::status ne WorkflowContext.Status.Done,
                MongoWorkflowBranch::waitingOn size 0,
                or(
                    and(
                        MongoWorkflowBranch::unlockAt exists true,
                        MongoWorkflowBranch::unlockAt eq null,
                    ),
                    MongoWorkflowBranch::unlockAt lte Instant.now()
                )
            ),
            setValue(
                getBranchesProperty().allPosOp / MongoWorkflowBranch::unlockAt,
                Instant.now().plus(Duration.ofMinutes(1))
            )
        )
    }

    override fun save(context: WorkflowContext) {
        context as MongoWorkflowContext

        context.updatedAt = Instant.now()

        collection.replaceOne(
            MongoWorkflowContext::id eq context.id,
            context,
            ReplaceOptions().also {
                it.upsert(true)
            }
        )
    }

    override fun delete(context: WorkflowContext) {
        collection.deleteOne(MongoWorkflowContext::id eq context.id)
    }

    override fun createFor(message: WorkflowMessage) {
        collection.insertOne(
            MongoWorkflowContext(
                WorkflowItem.extract(message),
                JSON.marshalString(message)
            )
        )
    }

    override fun resolveWaitingOn(signal: WorkflowSignal, state: WorkflowState) {
        collection.updateMany(
            MongoWorkflowContext::branches elemMatch (MongoWorkflowBranch::status ne WorkflowContext.Status.Done),
            combine(
                setValue(MongoWorkflowContext::updatedAt, Instant.now()),
                pullByFilter(
                    getBranchesProperty().allPosOp / MongoWorkflowBranch::waitingOn,
                    and(
                        WorkflowBranch.WaitingOn::state eq state.id,
                        WorkflowBranch.WaitingOn::signal eq signal.name
                    )
                )
            )
        )
    }

    @Suppress("UNCHECKED_CAST")
    private fun getBranchesProperty(): KProperty1<MongoWorkflowBranch, List<MongoWorkflowBranch>> {
        return (MongoWorkflowContext::branches as KProperty1<MongoWorkflowBranch, List<MongoWorkflowBranch>>)
    }
}