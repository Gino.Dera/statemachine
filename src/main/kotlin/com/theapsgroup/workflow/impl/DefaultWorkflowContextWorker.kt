package com.theapsgroup.workflow.impl

import com.theapsgroup.workflow.async.BackgroundTask
import com.theapsgroup.workflow.bus.WorkflowMessageBus
import com.theapsgroup.workflow.context.WorkflowContext
import com.theapsgroup.workflow.context.WorkflowContextProcessor
import com.theapsgroup.workflow.context.WorkflowContextRepository
import com.theapsgroup.workflow.core.DuringContextSaving
import com.theapsgroup.workflow.core.messageTypes
import com.theapsgroup.workflow.execution.WorkflowStepExecution
import com.theapsgroup.workflow.signal.WorkflowSignalRepository
import com.theapsgroup.workflow.util.JSON
import kotlinx.coroutines.delay

class DefaultWorkflowContextWorker(
    private val processor: WorkflowContextProcessor,
    private val contextRepository: WorkflowContextRepository,
    private val signalRepository: WorkflowSignalRepository,
    private val bus: WorkflowMessageBus
) : BackgroundTask {
    override suspend fun perform() {
        val context = contextRepository.getAvailable()
        if (context == null) {
            delay(1000)
            return
        }

        val branch = context.branches.firstOrNull {
            it.status != WorkflowContext.Status.Done && it.waitingOn.isEmpty()
        }

        if (branch != null) {
            val messageClass = messageTypes[context.type] ?: throw Exception(
                "Could not find message type: ${context.type}"
            )

            val message = JSON.unmarshal(messageClass, context.payload)

            val execution = WorkflowStepExecution(context, branch)
            processor.process(execution, context, branch, message)

            execution.messages.forEach {
                bus.publish(it)
            }

            execution.delayedActions[DuringContextSaving]?.forEach { func ->
                func()
            }

            if (context.branches.all { it.status == WorkflowContext.Status.Done }) {
                contextRepository.delete(context)
            } else {
                contextRepository.save(context)
            }

            if (execution.notifications.isNotEmpty()) {
                signalRepository.create(execution.notifications)
            }
        } else {
            println("No valid branch found")
        }
    }
}